<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Gives the user an input for entering a string to check if it is a palindrome
 *
 * @author Jason Roman <j@jayroman.com>
 */
class PalindromeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('string', null, array(
                'label' => 'String to Check',
                'constraints' => array(
                    new NotBlank(),
                ),
                'error_bubbling' => true,
            ))
            ->add('stripNonLetters', 'checkbox', array(
                'label' => 'Strip Non-Letters?',
                'required' => false,
            ))
        ;
    }

    public function getName()
    {
        return 'app_bundle_palindrome';
    }
}