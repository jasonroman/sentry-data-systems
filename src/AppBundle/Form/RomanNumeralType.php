<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Gives the user an input for entering a string to be converted to Roman Numeral
 *
 * @author Jason Roman <j@jayroman.com>
 */
class RomanNumeralType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('int', 'number', array(
                'label' => 'Number to Convert',
                'constraints' => array(
                    new NotBlank(),
                    new Range(array('min' => 1, 'max' => 3999)),
                ),
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'app_bundle_romannumeral';
    }
}