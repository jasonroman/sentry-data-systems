<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Gives the user an input for entering a string to be converted to an integer
 *
 * @author Jason Roman <j@jayroman.com>
 */
class AtoiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('int', null, array(
                'label' => 'String to Convert',
                'constraints' => array(
                    new NotBlank(),
                ),
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'app_bundle_atoi';
    }
}