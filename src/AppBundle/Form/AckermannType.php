<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

/**
 * Gives the user an input for computing an Ackermann value
 *
 * @author Jason Roman <j@jayroman.com>
 */
class AckermannType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('m', 'integer', array(
                'label' => 'M (value between 0 and 3)',
                'constraints' => array(
                    new NotBlank(),
                    new Range(array('min' => 0, 'max' => 3)),
                ),
                'error_bubbling' => true,
            ))
            ->add('n', 'integer', array(
                'label' => 'N (value between 0 and 4)',
                'constraints' => array(
                    new NotBlank(),
                    new Range(array('min' => 0, 'max' => 4)),
                ),
                'error_bubbling' => true,
            ))
        ;
    }

    public function getName()
    {
        return 'app_bundle_ackermann';
    }
}