<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\AckermannType;

/**
 * Ackermann controller
 *
 * @author Jason Roman <j@jayroman.com>
 *
 * @Route("/ackermann")
 */
class AckermannController extends Controller
{
    /**
     * Displays the form
     *
     * @Route("/", name="ackermann")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->createForm(new AckermannType());

        return array('form' => $form->createView());
    }

    /**
     * Handles the form submission
     *
     * @Route("/", name="ackermann_post")
     * @Method("POST")
     * @Template("AppBundle:Ackermann:index.html.twig")
     */
    public function ackermannAction(Request $request)
    {
        $error  = false;
        $result = null;

        $form = $this->createForm(new AckermannType());
        $form->handleRequest($request);

        if ($form->isValid())
        {
            try
            {
                $result = $this->get('ackermann')->ackermann(
                    $form->get('m')->getData(),
                    $form->get('n')->getData()
                );

                $this->addFlash('success', 'Value computed successfully! View the result below the form.');

            }
            catch (\Exception $e)
            {
                $error = true;
                echo $e->getMessage();
            }
        }
        else {
            echo "invalid";
        }

        if (!$form->isValid() || $error) {
            $this->addFlash('error', 'Please enter integer values from 0-3 for M and 0-4 for N.');
        }

        return array(
            'result'    => $result,
            'form'      => $form->createView(),
        );
    }
}