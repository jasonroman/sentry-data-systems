<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\RomanNumeralType;

/**
 * Roman Numeral controller
 *
 * @author Jason Roman <j@jayroman.com>
 *
 * @Route("/roman-numeral")
 */
class RomanNumeralController extends Controller
{
    /**
     * Displays the form
     *
     * @Route("/", name="roman_numeral")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->createForm(new RomanNumeralType());

        return array('form' => $form->createView());
    }

    /**
     * Handles the form submission
     *
     * @Route("/", name="roman_numeral_post")
     * @Method("POST")
     * @Template("AppBundle:RomanNumeral:index.html.twig")
     */
    public function romanNumeralAction(Request $request)
    {
        $error  = false;
        $result = null;

        $form = $this->createForm(new RomanNumeralType());
        $form->handleRequest($request);

        if ($form->isValid())
        {
            try
            {
                $result = $this->get('roman_numeral')->toRomanNumeral($form->get('int')->getData());

                $this->addFlash('success', 'Value converted successfully! View the result below the form.');

            } catch (\Exception $e) {
                $error = true;
            }
        }

        if (!$form->isValid() || $error) {
            $this->addFlash('error', 
                'Please enter a number (int or float) between 1 and 3999. '.
                'The decimal part must also be between 1 and 3999.'
            );
        }

        return array(
            'result'    => $result,
            'form'      => $form->createView(),
        );
    }
}