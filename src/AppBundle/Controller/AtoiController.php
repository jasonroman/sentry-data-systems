<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\AtoiType;

/**
 * Atoi controller
 *
 * @author Jason Roman <j@jayroman.com>
 *
 * @Route("/atoi")
 */
class AtoiController extends Controller
{
    /**
     * Displays the form
     *
     * @Route("/", name="atoi")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->createForm(new AtoiType());

        return array('form' => $form->createView());
    }

    /**
     * Handles the form submission
     *
     * @Route("/", name="atoi_post")
     * @Method("POST")
     * @Template("AppBundle:Atoi:index.html.twig")
     */
    public function atoiAction(Request $request)
    {
        $error  = false;
        $result = null;

        $form = $this->createForm(new AtoiType());
        $form->handleRequest($request);

        if ($form->isValid())
        {
            try
            {
                $result = $this->get('atoi')->atoi($form->get('int')->getData());

                $this->addFlash('success', 'Value converted successfully! View the result below the form.');

            } catch (\Exception $e) {
                $error = true;
            }
        }

        if (!$form->isValid() || $error) {
            $this->addFlash('error', 'Please enter a string that equates to an integer.');
        }

        return array(
            'result'    => $result,
            'form'      => $form->createView(),
        );
    }
}