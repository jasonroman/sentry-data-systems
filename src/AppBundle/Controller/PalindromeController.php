<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\PalindromeType;

/**
 * Palindrome controller
 *
 * @author Jason Roman <j@jayroman.com>
 *
 * @Route("/palindrome")
 */
class PalindromeController extends Controller
{
    /**
     * Displays the form
     *
     * @Route("/", name="palindrome")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->createForm(new PalindromeType());

        return array('form' => $form->createView());
    }

    /**
     * Handles the form submission
     *
     * @Route("/", name="palindrome_post")
     * @Method("POST")
     * @Template("AppBundle:Palindrome:index.html.twig")
     */
    public function palindromeAction(Request $request)
    {
        $error  = false;
        $result = null;

        $form = $this->createForm(new PalindromeType());
        $form->handleRequest($request);

        if ($form->isValid())
        {
            try
            {
                $result = $this->get('palindrome')->isPalindrome(
                    $form->get('string')->getData(),
                    $form->get('stripNonLetters')->getData()
                );

                $this->addFlash('success', 'Value checked successfully! View the result below the form.');

            } catch (\Exception $e) {
                $error = true;
            }
        }

        if (!$form->isValid() || $error) {
            $this->addFlash('error', 'Please enter a string.');
        }

        return array(
            'result'    => $result,
            'form'      => $form->createView(),
        );
    }
}