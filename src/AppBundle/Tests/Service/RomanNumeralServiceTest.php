<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\RomanNumeralService;

/**
 * Unit tests for the RomanNumeralService
 *
 * @author Jason Roman <j@jayroman.com>
 */
class RomanNumeralServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var RomanNumeral Service
     */
    private static $service;

    /**
     * Runs once before entire suite of tests
     */
    public static function setUpBeforeClass()
    {
        self::$service = new RomanNumeralService();
    }

    /**
     * Runs once after entire suite of tests
     */
    public static function tearDownAfterClass()
    {
        self::$service = null;
    }

    /**
     * @dataProvider integerProvider
     *
     * @param int $number
     * @param string $expected
     */
    public function testToRomanNumeralInteger($number, $expected)
    {
        $this->assertSame($expected, self::$service->toRomanNumeral($number));
    }

    /**
     * @dataProvider floatProvider
     *
     * @param float $number
     * @param string $expected
     */
    public function testToRomanNumeralFloat($number, $expected)
    {
        $this->assertSame($expected, self::$service->toRomanNumeral($number));
    }

    /**
     * @dataProvider nonPositiveNumberProvider
     *
     * @expectedException \Exception
     * @expectedExceptionMessage must be a positive number less than or equal to 3999
     *
     * @param mixed $nonPositiveInteger
     */
    public function testToRomanNumeralException($nonPositiveNumber)
    {
        self::$service->toRomanNumeral($nonPositiveNumber);
    }

    /**
     * Returns integer values and their Roman Numeral equivalent
     *
     * @return array
     */
    public function integerProvider()
    {
        return array(
            // matching the traditional conversion values
            array(1, 'I'),
            array(4, 'IV'),
            array(5, 'V'),
            array(9, 'IX'),
            array(10, 'X'),
            array(40, 'XL'),
            array(50, 'L'),
            array(90, 'XC'),
            array(100, 'C'),
            array(400, 'CD'),
            array(500, 'D'),
            array(900, 'CM'),
            array(1000, 'M'),
            // matching other random values
            array(99, 'XCIX'),
            array(217, 'CCXVII'),
            array(249, 'CCXLIX'),
            array(450, 'CDL'),
            array(1188, 'MCLXXXVIII'),
            array(3999, 'MMMCMXCIX'),
        );
    }

    /**
     * Returns float values and their Roman Numeral equivalent
     *
     * @return array
     */
    public function floatProvider()
    {
        return array(
            array(1.0, 'I'),
            array(4.1, 'IV.I'),
            array(5.25, 'V.XXV'),
            array(405.100, 'CDV.I'),
            array(500.001, 'D.00I'),
            array(917.003, 'CMXVII.00III'),
            array(2450.2450, 'MMCDL.CCXLV'),
            array(3998.1188, 'MMMCMXCVIII.MCLXXXVIII'),
        );
    }

    /**
     * Returns non-positive-integer values
     *
     * @return array
     */
    public function nonPositiveNumberProvider()
    {
        return array(
            array(-1),
            array(0),
            array(null),
            array(new \stdClass()),
            array(array('not a string')),
            array(4000),        // errors if value over 3999 given
            array(1000.4001),   // errors if decimal value over 3999 given
        );
    }
}