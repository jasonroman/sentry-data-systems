<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\PalindromeService;

/**
 * Unit tests for the PalindromeService
 *
 * @author Jason Roman <j@jayroman.com>
 */
class PalindromeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PalindromeService
     */
    private static $service;

    /**
     * Runs once before entire suite of tests
     */
    public static function setUpBeforeClass()
    {
        self::$service = new PalindromeService();
    }

    /**
     * Runs once after entire suite of tests
     */
    public static function tearDownAfterClass()
    {
        self::$service = null;
    }

    /**
     * @dataProvider isPalindromeStrictProvider
     *
     * @param string $string
     * @param string $expected
     */
    public function testIsPalindromeStrict($string, $expected)
    {
        // compare against the traditional palindrome function to double-check correctness
        $this->assertSame($expected, self::$service->isPalindrome($string));
        $this->assertSame($expected, self::$service->isPalindromeTraditional($string));
    }

    /**
     * @dataProvider isPalindromeNonStrictProvider
     *
     * @param string $string
     * @param string $expected
     */
    public function testIsPalindromeNonStrict($string, $expected)
    {
        // strips non-letters and runs the palindrome test
        $this->assertSame($expected, self::$service->isPalindrome($string, true));
    }

    /**
     * @dataProvider nonStringProvider
     *
     * @expectedException \Exception
     * @expectedExceptionMessage must be a string
     *
     * @param mixed $string
     */
    public function testIsPalindromeException($nonString)
    {
        self::$service->isPalindrome($nonString);
    }

    /**
     * Returns strings and their expected value when checking if a palindrome
     *
     * @return array
     */
    public function isPalindromeStrictProvider()
    {
        return array(
            array('TestString', false),
            array('NotaPalindrome', false),
            array('OhNoThisIsNoPalindrome', false),
            array('Mom', true),
            array('RaCeCar', true),
            array('AcrobatsStabOrca', true),
            array('ANutForAJarOfTuna', true),
        );
    }

    /**
     * Returns strings and their expected value when checking if a palindrome when non-letters are stripped
     *
     * @return array
     */
    public function isPalindromeNonStrictProvider()
    {
        return array(
            array('TestString', false),
            array('NotaPalindrome', false),
            array('OhNoThisIsNoPalindrome', false),
            array('Mom', true),
            array('R a Ce C a r', true),
            array('Acrobats. Stab. Orca.', true),
            array('A nut, for a jar of tuna????', true),
        );
    }

    /**
     * Returns non-string values
     *
     * @return array
     */
    public function nonStringProvider()
    {
        return array(
            array(1001),
            array(null),
            array(new \stdClass()),
            array(array('not a string')),
        );
    }
}