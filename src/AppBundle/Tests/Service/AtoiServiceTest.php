<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\AtoiService;

/**
 * Unit tests for the AtoiService
 *
 * @author Jason Roman <j@jayroman.com>
 */
class AtoiServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AtoiService
     */
    private static $service;

    /**
     * Runs once before entire suite of tests
     */
    public static function setUpBeforeClass()
    {
        self::$service = new AtoiService();
    }

    /**
     * Runs once after entire suite of tests
     */
    public static function tearDownAfterClass()
    {
        self::$service = null;
    }

    /**
     * @dataProvider integerStringProvider
     *
     * @param string $string
     * @param int $expected
     */
    public function testAtoi($string, $expected)
    {
        // also test against a traditional type-cast to double check correctness
        $this->assertSame($expected, self::$service->atoi($string));
        $this->assertSame((int) $string, self::$service->atoi($string));
    }

    /**
     * @dataProvider nonStringProvider
     *
     * @expectedException \Exception
     * @expectedExceptionMessage must be a string
     *
     * @param mixed $nonIntegerString
     */
    public function testToRomanNumeralExceptionNotString($nonIntegerString)
    {
        self::$service->atoi($nonIntegerString);
    }

    /**
     * @dataProvider nonIntegerStringProvider
     *
     * @expectedException \Exception
     * @expectedExceptionMessage invalid non-digit characters
     *
     * @param mixed $nonIntegerString
     */
    public function testToRomanNumeralExceptionNotIntegerString($nonIntegerString)
    {
        self::$service->atoi($nonIntegerString);
    }

    /**
     * Returns strings that are castable to integers
     *
     * @return array
     */
    public function integerStringProvider()
    {
        return array(
            array('001', 1),
            array('-001', -1),
            array('20', 20),
            array('100', 100),
            array('99384', 99384),
            array('9520234', 9520234),
            array('101209812094', 101209812094),
        );
    }

    /**
     * Returns non-string values
     *
     * @return array
     */
    public function nonStringProvider()
    {
        return array(
            array(1001),
            array(null),
            array(new \stdClass()),
            array(array('not a string')),
        );
    }

    /**
     * Returns non-integer-string values
     *
     * @return array
     */
    public function nonIntegerStringProvider()
    {
        return array(
            array('9adkj21'),
            array('100lIl'),
            array('@)(*#$)(J'),
            array('LKJ(@(!+++++/\d\f'),
        );
    }
}