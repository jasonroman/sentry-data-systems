<?php

namespace AppBundle\Tests\Service;

use AppBundle\Service\AckermannService;

/**
 * Unit tests for the AckermannService
 *
 * @author Jason Roman <j@jayroman.com>
 */
class AckermannServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AckermannService
     */
    private static $service;

    /**
     * Runs once before entire suite of tests
     */
    public static function setUpBeforeClass()
    {
        self::$service = new AckermannService();
    }

    /**
     * Runs once after entire suite of tests
     */
    public static function tearDownAfterClass()
    {
        self::$service = null;
    }

    /**
     * @dataProvider ackermannProvider
     *
     * @param int $m
     * @param int $n
     * @param int $expected
     */
    public function testAckermann($m, $n, $expected)
    {
        $this->assertSame($expected, self::$service->ackermann($m, $n));
        $this->assertSame($expected, self::$service->ackermannTraditional($m, $n));
    }

    /**
     * @dataProvider invalidMProvder
     *
     * @expectedException \Exception
     * @expectedExceptionMessage M must be a nonnegative integer less than or equal to 3
     *
     * @param int $m
     * @param int $n
     */
    public function testAckermannInvalidMException($m, $n)
    {
        self::$service->ackermann($m, $n);
    }

    /**
     * @dataProvider invalidNProvder
     *
     * @expectedException \Exception
     * @expectedExceptionMessage N must be a nonnegative integer less than or equal to 4
     *
     * @param int $m
     * @param int $n
     */
    public function testAckermannInvalidNException($m, $n)
    {
        self::$service->ackermann($m, $n);
    }

    /**
     * Returns m and n values and the expected result of the Ackermann function
     *
     * @return array
     */
    public function ackermannProvider()
    {
        return array(
            array(0, 0, 1),
            array(0, 1, 2),
            array(0, 2, 3),
            array(0, 3, 4),
            array(0, 4, 5),
            array(1, 0, 2),
            array(1, 1, 3),
            array(1, 2, 4),
            array(1, 3, 5),
            array(1, 4, 6),
            array(2, 0, 3),
            array(2, 1, 5),
            array(2, 2, 7),
            array(2, 3, 9),
            array(2, 4, 11),
            array(3, 0, 5),
            array(3, 1, 13),
            array(3, 2, 29),
            array(3, 3, 61),
            array(3, 4, 125),
        );
    }

    /**
     * Returns m values that are invalid - not integers, or not in the range 0 to 3
     *
     * @return array
     */
    public function invalidMProvder()
    {
        return array(
            array(-1, 0),
            array(4, 2),
            array('string', 3),
            array(new \stdClass(), 3),
        );
    }

    /**
     * Returns n values that are invalid - not integers, or not in the range 0 to 4
     *
     * @return array
     */
    public function invalidNProvder()
    {
        return array(
            array(0, -1),
            array(1, 5),
            array(3, 'string'),
            array(2, new \stdClass()),
        );
    }
}