<?php

namespace AppBundle\Tests\Integration;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Example of integration tests for each of the bundle's services;
 * Uses the Symfony container so the actual services are called rather than instantiating the classes directly
 *
 * @author Jason Roman <j@jayroman.com>
 */
class ServicesTest extends KernelTestCase
{
    /**
     * @var ContainerInterface
     */
    private static $container;

    /**
     * Runs once per entire suite of tests and sets up the Symfony kernel and container
     */
    public static function setUpBeforeClass()
    {
        // set up the Symfony container so services can be called
        self::bootKernel();
        self::$container = static::$kernel->getContainer();
    }

    /**
     * Runs once after entire suite of tests
     */
    public static function tearDownAfterClass()
    {
        // shut down the Symfony kernel and clear the container from memory
        static::ensureKernelShutdown();

        self::$container = null;
    }

    /**
     * Empty function to override KernelTestCase so the kernel is not shut down after every individual test
     */
    protected function tearDown()
    {
    }

    /**
     * Test that the Ackermann service can be called
     */
    public function testAckermann()
    {
        $this->assertSame(11, self::$container->get('ackermann')->ackermann(2, 4));
    }

    /**
     * Test that the Atoi service can be called
     */
    public function testAtoi()
    {
        $this->assertSame(-501, self::$container->get('atoi')->atoi('-501'));
        $this->assertSame(100, self::$container->get('atoi')->atoi('100'));
    }

    /**
     * Test that the Palindrome service can be called
     */
    public function testPalindrome()
    {
        $this->assertTrue(self::$container->get('palindrome')->isPalindrome('AManAPlanACanalPanama'));
        $this->assertTrue(self::$container->get('palindrome')->isPalindrome('A man. A plan. A Canal. Panama.', true));
        $this->assertFalse(self::$container->get('palindrome')->isPalindrome('NoPalindrome'));
        $this->assertFalse(self::$container->get('palindrome')->isPalindrome('NoPalindrome', true));
    }

    /**
     * Test that the RomanNumeral service can be called
     */
    public function testRomanNumeral()
    {
        $this->assertSame('MCLXXXVIII', self::$container->get('roman_numeral')->toRomanNumeral(1188));
        $this->assertSame('D.00I', self::$container->get('roman_numeral')->toRomanNumeral(500.001));
    }
}