<?php

namespace AppBundle\Service;

/**
 * Service class for converting integers to roman numerals
 *
 * @author Jason Roman <j@jayroman.com>
 */
class RomanNumeralService
{
    /**
     * @var array
     */
    private $conversion;

    /**
     * @var array
     */
    private $values;

    /**
     * @var array
     */
    private $numerals;

    /**
     * Constructor - initializes the conversion array and separates into values and numerals for ease of use
     */
    public function __construct()
    {
        // reverse the conversion string - listed ascending here for clarity
        $this->conversion = array_reverse(array(
            'I'  => 1,
            'IV' => 4,
            'V'  => 5,
            'IX' => 9,
            'X'  => 10,
            'XL' => 40,
            'L'  => 50,
            'XC' => 90,
            'C'  => 100,
            'CD' => 400,
            'D'  => 500,
            'CM' => 900,
            'M'  => 1000,
        ));

        // separate the conversion array into the Roman Numerals and corresponding values
        $this->numerals = array_keys($this->conversion);
        $this->values   = array_values($this->conversion);
    }

    /**
     * Implement integer-to-Roman numeral formatting function:
     *  - permits assigning symbols for values other than 1 and 5 without changing the code
     *  - can represent floating point numbers
     *
     * Encoding scheme for floating point numbers:
     *  - Format: <standard_conversion>.<leading_zeroes><standard_conversion> (ex: 5.04 => V.0IV)
     *  - any leading zeroes will remain zeros (ex: 5.005 => V.00V)
     *  - any trailing zeroes will be treated as if they are not there (ex: 5.100 = 5.1)
     *  - the remaining portion of the decimal value will be converted as if it were a regular number
     *
     * @param int|float $number
     *
     * @return string
     *
     * @throws \Exception
     */
    public function toRomanNumeral($number)
    {
        $exceptionMessage = 'Value (and decimal part) must be a positive number less than or equal to 3999';

        // make sure the main number is valid
        if (!is_numeric($number) || $number <= 0 || $number > 3999) {
            throw new \Exception($exceptionMessage);
        }

        // check if this is a decimal number by comparing the floor
        $isFloatingPoint = ($number != (int) $number);

        // convert the main number to roman numberal and if this isn't a floating point value, return the result
        $result = $this->numberToRomanNumeral((int) $number);

        if (!$isFloatingPoint) {
            return $result;
        }

        // this is floating point, so add the decimal
        $result .= '.';

        // get the decimal part of the number and make sure the number is valid
        list($junk, $decimalPart) = (explode('.', (string) $number));

        if ((int) $decimalPart > 3999) {
            throw new \Exception($exceptionMessage);
        }

        $leadingZeroes = true;
        $decimalNumber = '';

        for ($i = 0; $i < strlen($decimalPart); $i++)
        {
            // retain the leading zeroes
            if ($decimalPart[$i] == '0') {
                $result .= '0';
            }
            else {
                $leadingZeroes = false;
            }

            // continue creating the decimal part to convert as a string
            if (!$leadingZeroes) {
                $decimalNumber .= $decimalPart[$i];
            }
        }

        // convert the number after the leading zeroes and append to the result;
        // for example, if the decimal part is .00515 the decimal number is 515
        $result .= $this->numberToRomanNumeral((int) $decimalNumber);

        return $result;
    }

    /**
     * Convert a given number to a Roman Numeral
     *
     * @param int $number
     *
     * @return string
     */
    private function numberToRomanNumeral($number)
    {
        $result = '';

        // go through the conversion array, and start with the highest possible value;
        // if the number is higher than that, add the corresponding Roman numeral, subtract that number, and repeat
        for ($i = 0; $i < count($this->conversion); $i++)
        {
            while ($number >= $this->values[$i])
            {
                $number -= $this->values[$i];
                $result .= $this->numerals[$i];
            }
        }

        return $result;
    }
}