<?php

namespace AppBundle\Service;

/**
 * Service class for the equivalent of C's atoi() function
 *
 * @author Jason Roman <j@jayroman.com>
 */
class AtoiService
{
    /**
     * Implement atoi() to convert a string to an integer:
     *  - does not use built-in language functions
     *  - does not use typecasting
     *  - does not use multiplication
     *  - uses as few conditionals as possible
     *
     * @param string $int
     *
     * @return int
     *
     * @throws \Exception
     */
    public function atoi($int)
    {
        if (!is_string($int)) {
            throw new \Exception('Value to check must be a string');
        }

        $i              = 0;
        $sign           = 1;
        $result         = 0;
        $leadingZeroes  = true;

        // if the string starts with '-', consider it a negative integer and convert the sign;
        // bump the value of 'i' so the leading zeroes check doesn't take the dash into account
        if ($int[0] == '-')
        {
            $sign = -1;
            $i++;
        }

        // loop through every character of the string
        for ($i; $i < strlen($int); $i++)
        {
            // ignore leading zeroes - set to false on the first non-zero digit in the string
            if ($leadingZeroes && $int[$i] == '0') {
                continue;
            }
            else {
                $leadingZeroes = false;
            }

            // if this is a non-0-to-9 ASCII value, throw an exception
            if (ord($int[$i]) < 48 || ord($int[$i]) > 57) {
                throw new \Exception('Value contains invalid non-digit characters');
            }
            // otherise convert the ASCII value to its number and add it to the result
            // first multiply the result by 10 to deal with each successive digit in the string
            else {
                $result = ($result * 10) + (ord($int[$i]) - 48);
            }
        }

        // return the result with the proper positive/negative designation
        return $result * $sign;
    }
}