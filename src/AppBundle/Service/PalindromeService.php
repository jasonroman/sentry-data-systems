<?php

namespace AppBundle\Service;

/**
 * Service class for Palindrome functions
 *
 * @author Jason Roman <j@jayroman.com>
 */
class PalindromeService
{
    /**
     * Determines whether a string is a palindrome:
     *  - using constant memory
     *  - accessing each character in the string only once
     *
     * @param string $string
     * @param bool $stripNonLetters
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function isPalindrome($string, $stripNonLetters = false)
    {
        if (!is_string($string)) {
            throw new \Exception('Value to check must be a string');
        }

        // make this check case-insensitive
        $string = strtolower($string);

        // strip non-letters if specified (ex: "a man, a plan, a canal: panama." becomes amanaplanacanalpanama)
        if ($stripNonLetters) {
            $string = preg_replace("/[^a-z]/i", '', $string);
        }
        
        $length = strlen($string);

        // return false on the first non-match; if length is odd it simply skips the middle value
        for ($i = 0; $i < $length / 2; $i++)
        {
            if ($string[$i] !== $string[$length - 1 - $i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Determines whether a string is a palindrome using PHP functions
     *
     * @param string $string
     *
     * @return bool
     */
    public function isPalindromeTraditional($string)
    {
        $string = strtolower($string);

        return ($string === strrev($string));
    }
}