<?php

namespace AppBundle\Service;

/**
 * Service class for Ackermann's function
 *
 * @author Jason Roman <j@jayroman.com>
 */
class AckermannService
{
    /**
     * Implement Ackermann's function:
     *  - does not recompute already solved values
     *  - optimizes low values of m as special cases
     *  - without using any recursive function calls
     *
     * @param int $m
     * @paran int $n
     *
     * @return int
     *
     * @throws \Exception
     */
    public function ackermann($m, $n)
    {
        // make sure the input is valid
        if (!is_int($m) || $m < 0 || $m > 3) {
            throw new \Exception('Value for M must be a nonnegative integer less than or equal to 3');
        }
        elseif (!is_int($n) || $n < 0 || $n > 4) {
            throw new \Exception('Value for N must be a nonnegative integer less than or equal to 4');
        }

        // simple optimization for m values of 0 or 1
        if ($m == 0 || $m == 1) {
            return $n + $m + 1;
        }

        // here, m is 1 or 2, simply take the non-recursive functions to produce these values
        if ($m == 2) {
            return 2 * ($n + 3) - 3;
        }
        else {
            return pow(2, $n + 3) - 3;
        }
    }

    /**
     * Runs Ackermann's function using traditional recursion
     *
     * @param int $m
     * @paran int $n
     *
     * @return int
     */
    public function ackermannTraditional($m, $n)
    {
        if ($m == 0) {
            return $n + 1;
        }
        elseif ($n == 0) {
            return $this->ackermannTraditional($m - 1, 1);
        }

        return $this->ackermannTraditional($m - 1, $this->ackermannTraditional($m, $n - 1));
    }
}