Sentry Data Systems Test - Jason Roman
================

This is the Sentry Data Systems coding test for developers.  It contains answers to the stated problems, and does so in the context of a full-fledged Symfony2 application.  There are 4 main files that solve the problems as well as unit tests to verify the solutions.  The rest of the files source are for website that utilizes these services.

### Code

Each of the 4 problems is contained within a PHP class in this repository.  The source code for each can be found here:

* **src/AppBundle/Service/AckermannService.php**
* **src/AppBundle/Service/AtoiService.php**
* **src/AppBundle/Service/PalindromeService.php**
* **src/AppBundle/Service/RomanNumeralService.php**

Unit tests for these files can be found in the following directories:

* **src/AppBundle/Tests/Integration** - tests with the classes used as Symfony services
* **src/AppBundle/Tests/Service** - tests using traditional PHPUnit methods

The other relevant places to look for code and configuration files are:

* **app/Resources** - contains the base, footer, and navigation Twig templates
* **app/config/config.yml** - contains the asset and Bootstrap form configuration
* **app/config/config_prod.yml** - sets the automatic compressing of Javascript and CSS files
* **config/** - houses the Apache configuration files
* **src/AppBundle/Controller** - contains the controller classes for each page
* **src/AppBundle/DependencyInjection** - handles automatic loading of bundle services
* **src/AppBundle/Form** - contains all of the form classes for each service along with constraints
* **src/AppBundle/Resources** - defines service definitions, public assets and Twig templates

### Running the Unit Tests

Clone the website to a local directory with the following command:

    cd /path/to/your/base/directory
    git clone https://jasonroman@bitbucket.org/jasonroman/sentry-data-systems.git sentry-data-systems

This will create a **/path/to/your/base/directory/sentry-data-systems** folder containing all of the source code.  You can then run the the suite of the unit tests via:

    phpunit -c app/ src/AppBundle/Tests/

Running these tests should give a result of **OK (99 tests, 165 assertions)**

### Website

To simply view the functionality on the website, visit:

http://sds.jayroman.com

This contains the 4 problems where you may submit your inputs via forms and return the results.  This automatically
handles errors.  The website demonstrates the following capabilities:

* Creating a Symfony2 Application
* Creating an Apache configuration for the website as a subdomain deployed in a production environment
* Knowledge of Twitter Bootstrap and responsive web design that functions equally well on tablets/mobile devices
* Asset management - versioning and minification
* Form handling - submission, retrieving data, Symfony Constraints, and configuration
* Calling Symfony services
* Using Session Flash messages
* Using Git for version control

If you have any further questions, feel free to contact me at:

<j@jayroman.com> // 586-634-6308
